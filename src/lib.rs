extern crate add_one;
extern crate rand;

pub fn hi() -> i32 {
    add_one::add_one(3)
}

pub fn random_hi() -> i32 {
    add_one::add_one(rand::random())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(hi(), 4);
    }
}
